import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Tenant } from './tenant';
import { TenantService } from './tenant.service';

@Component({
  selector: 'tenant-add',
  templateUrl: './tenant-add.component.html',
  styleUrls: ['./tenant-add.component.css']
})
export class TenantAddComponent implements OnInit {

  tenantForm = new FormGroup ({
    uri: new FormControl("", Validators.required),
    name: new FormControl("", Validators.required),
    description: new FormControl()
  });

  submitted = false;

  constructor(private router: Router, private formBuilder: FormBuilder, private tenantService: TenantService) { 
    this.createForm();
  }

  ngOnInit(): void {
    
  }

  createForm() {
    this.tenantForm = this.formBuilder.group({
      name: '',
      uri: '',
      description: ''
    });
  }

  onSubmit() {
    this.tenantService.newTenant(<Tenant>this.tenantForm.value).subscribe(
      response => {
        console.log(response);
        this.submitted = true;
        this.router.navigateByUrl("/tenants");
      },
      error => {
        console.log(error);
      });
  }

}
