import { Component, OnInit } from '@angular/core';

import { Tenant } from './tenant';
import { TenantService } from './tenant.service';

@Component({
  selector: 'tenant-list',
  templateUrl: './tenant-list.component.html',
  styleUrls: ['./tenant-list.component.css']
})
export class TenantListComponent implements OnInit {
  tenants: Tenant[];  

  displayedColumns: string[] = ['uri','name'];

  constructor(private tenantService: TenantService) { 
    this.tenants = [];
  }

  ngOnInit(): void {
    this.getTenants();
  }

  getTenants(): void {
    this.tenantService.getTenants().subscribe(tenants => this.tenants = tenants);
  }

}
