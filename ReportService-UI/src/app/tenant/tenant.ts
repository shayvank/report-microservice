export interface Tenant {
    uri: string;
    name: string;
    description: string;
    version: number;
}