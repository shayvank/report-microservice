import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Tenant } from "./tenant"
import { identifierModuleUrl } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class TenantService {

  private theUrl = 'api/tenants';

  constructor(private http: HttpClient) { }

  getTenants(): Observable<Tenant[]> {
    return this.http.get<Tenant[]>(this.theUrl);
  }

  getTenantById(id: string): Observable<Tenant> {
    return this.http.get<Tenant>(this.theUrl + "/" + id);
  }

  newTenant(data: Tenant): Observable<Tenant> {
    return this.http.post<Tenant>(this.theUrl, data);
  }
}
