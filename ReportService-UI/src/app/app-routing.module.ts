import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TenantAddComponent } from './tenant/tenant-add.component';
import { TenantListComponent } from './tenant/tenant-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'tenants', pathMatch: 'full'},
  { path: 'tenants', component: TenantListComponent },
  { path: 'tenants/add', component: TenantAddComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
