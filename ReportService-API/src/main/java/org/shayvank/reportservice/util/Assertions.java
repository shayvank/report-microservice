package org.shayvank.reportservice.util;

import org.springframework.util.Assert;

public class Assertions {

	private Assertions() {
		// private constructor, static class
	}
	
	public static void hasText(final String text, final String message) {
		Assert.hasText(text, message);
	}
	
	public static void rangeLength(final String text, final int minimum, final int maximum, final String message) {
		String theText = text;
		if (theText == null) {
			if (minimum == 0) {
				theText = "";
			} else {
				throw new IllegalArgumentException(message);
			}
		}
		int length = theText.trim().length();
		if (length < minimum || length > maximum) {
			throw new IllegalArgumentException(message);
		}
	}
}
