package org.shayvank.reportservice.domain.model.tenant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import org.shayvank.reportservice.domain.model.EntityUtils;
import org.shayvank.reportservice.domain.model.Versioned;
import org.shayvank.reportservice.util.Assertions;

@Entity
@Table(name = "TENANT")
public class Tenant implements Versioned {

	@Id
	@GeneratedValue(
			generator = "tenant_sequence_id")
	@SequenceGenerator(
			name = "tenant_sequence_id",
			sequenceName = "TENANT_SEQUENCE",
			allocationSize = 1)
	private Long id;
	
	@Column(name = "URI")
	private String uri;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Version
	@Column(name = "VERSION")
	private Long version;
	
	protected Tenant() {}
	
	public Tenant(final String aUri, final String aName, final String aDescription) {
		changeUriInternal(aUri);
		changeNameInternal(aName);
		changeDescriptionInternal(aDescription);
	}
	
	public String description() {
		return description;
	}
	
	public Long id() {
		return id;
	}
	
	public String name() {
		return name;
	}
	
	public String uri() {
		return uri;
	}
	
	@Override
	public Long getVersion() {
		return version;
	}
	
	private void changeUriInternal(final String aUri) {
		Assertions.hasText(aUri, "Tenant uri must have a value.");
		Assertions.rangeLength(aUri, 1, 55, "Tenant uri must be at least 1 character but not more than 55 characters.");
		this.uri = aUri;
	}
	
	public void changeUri(final String aUri, final Long version) {
		changeUriInternal(aUri);
		EntityUtils.performConcurrencyCheck(this, version);
	}
	
	private void changeNameInternal(final String aName) {
		Assertions.hasText(aName, "Tenant name must have a value.");
		Assertions.rangeLength(aName, 1, 55, "Tenant name must be at least 1 character but not more than 55 characters.");		
		this.name = aName;
	}
	
	public void changeName(final String aName, final Long version) {
		changeNameInternal(aName);
		EntityUtils.performConcurrencyCheck(this, version);
	}
	
	private void changeDescriptionInternal(final String aDescription) {
		Assertions.rangeLength(aDescription, 0, 128, "Tenant description must not be more than 128 characters.");
		this.description = aDescription;
	}
	
	public void changeDescription(final String aDescription, final Long version) {
		changeDescriptionInternal(aDescription);
		EntityUtils.performConcurrencyCheck(this, version);
	}
		
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tenant other = (Tenant) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
