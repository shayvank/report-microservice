package org.shayvank.reportservice.domain.model.artifact;

import java.util.List;

public interface FileArtifactCrudJpaRepository extends ArtifactBaseRepository<FileArtifact> {

	List<FileArtifact> findByTenantId(Long tenantId);
	
	FileArtifact findByUriAndTenantId(String uri, Long tenantId);
}
