package org.shayvank.reportservice.domain.model;

public interface Versioned {

	Long getVersion();
}
