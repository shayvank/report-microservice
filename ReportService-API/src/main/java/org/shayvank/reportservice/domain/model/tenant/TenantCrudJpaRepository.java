package org.shayvank.reportservice.domain.model.tenant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TenantCrudJpaRepository extends JpaRepository<Tenant, Long> {

	Tenant findByUri(String uri);
	
	Tenant findByName(String name);
	
	Tenant findByIdAndVersion(Long id, Long version);
	
	Tenant findByUriAndVersion(String uri, Long version);
}
