package org.shayvank.reportservice.domain.model.tenant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TenantRoleCrudJpaRepository extends JpaRepository<TenantRole, Long> {

	TenantRole findByUriAndTenantId(String uri, Long tenantId);
}
