package org.shayvank.reportservice.domain.model.artifact;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import org.shayvank.reportservice.domain.model.EntityUtils;
import org.shayvank.reportservice.domain.model.Versioned;
import org.shayvank.reportservice.util.Assertions;

@Entity
@Table(name = "ARTIFACT")
@Inheritance(strategy = InheritanceType.JOINED)
public class Artifact implements Versioned {

	@Id
	@GeneratedValue(
			generator = "artifact_sequence_id")
	@SequenceGenerator(
			name = "artifact_sequence_id",
			sequenceName = "ARTIFACT_SEQUENCE",
			allocationSize = 1)
	private Long id;
	
	@Column(name = "URI")
	private String uri;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "RES_TYPE")
	protected ArtifactType artifactType;
	
	@Column(name = "TENANT_ID")
	private Long tenantId;
	
	protected Artifact() {}
	
	public Artifact(final String aUri, final String aName, final String aDescription, final ArtifactType artifactType, final Long tenantId) {
		changeUriInternal(aUri);
		changeNameInternal(aName);
		changeDescriptionInternal(aDescription);
		this.artifactType = artifactType;
		this.tenantId = tenantId;
	}
	
	public String description() {
		return description;
	}
	
	public Long id() {
		return id;
	}
	
	public String name() {
		return name;
	}
	
	public String uri() {
		return uri;
	}
	
	public ArtifactType getArtifactType() {
		return artifactType;
	}
	
	public Long getTenantId() {
		return tenantId;
	}
	
	@Version
	@Column(name = "VERSION")
	private Long version;
	
	@Override
	public Long getVersion() {
		return version;
	}
	
	private void changeUriInternal(final String aUri) {
		Assertions.hasText(aUri, "Artifact uri must have a value.");
		Assertions.rangeLength(aUri, 1, 55, "Artifact uri must be at least 1 character but not more than 55 characters.");
		this.uri = aUri;
	}
	
	public void changeUri(final String aUri, final Long version) {
		changeUriInternal(aUri);
		EntityUtils.performConcurrencyCheck(this, version);
	}
	
	private void changeNameInternal(final String aName) {
		Assertions.hasText(aName, "Artifact name must have a value.");
		Assertions.rangeLength(aName, 1, 55, "Artifact name must be at least 1 character but not more than 55 characters.");		
		this.name = aName;
	}
	
	public void changeName(final String aName, final Long version) {
		changeNameInternal(aName);
		EntityUtils.performConcurrencyCheck(this, version);
	}
	
	private void changeDescriptionInternal(final String aDescription) {
		Assertions.rangeLength(aDescription, 0, 128, "Artifact description must not be more than 128 characters.");
		this.description = aDescription;
	}
	
	public void changeDescription(final String aDescription, final Long version) {
		changeDescriptionInternal(aDescription);
		EntityUtils.performConcurrencyCheck(this, version);
	}

}
