package org.shayvank.reportservice.domain.model.artifact;

import java.util.List;

public interface DataSourceArtifactCrudJpaRepository extends ArtifactBaseRepository<DataSourceArtifact> {

	List<DataSourceArtifact> findByTenantId(Long tenantId);
	
	DataSourceArtifact findByUriAndTenantId(String uri, Long tenantId);
}
