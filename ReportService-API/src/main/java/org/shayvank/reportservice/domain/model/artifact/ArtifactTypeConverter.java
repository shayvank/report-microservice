package org.shayvank.reportservice.domain.model.artifact;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class ArtifactTypeConverter implements AttributeConverter<ArtifactType, String> {

	@Override
	public String convertToDatabaseColumn(final ArtifactType attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.getCode();
	}

	@Override
	public ArtifactType convertToEntityAttribute(final String code) {
        if (code == null) {
            return null;
        }

        return Stream.of(ArtifactType.values())
          .filter(c -> c.getCode().equals(code))
          .findFirst()
          .orElseThrow(IllegalArgumentException::new);
	}

}
