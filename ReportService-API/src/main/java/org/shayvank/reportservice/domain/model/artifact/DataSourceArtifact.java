package org.shayvank.reportservice.domain.model.artifact;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.shayvank.reportservice.domain.model.EntityUtils;
import org.shayvank.reportservice.util.Assertions;

@Entity
@Table(name = "DATASOURCE_ARTIFACT")
@PrimaryKeyJoinColumn(name="ARTIFACT_ID")
public class DataSourceArtifact extends Artifact {

	@Column(name = "JNDI_NAME")
	private String jndiName;
	
	protected DataSourceArtifact() {}
	
	public DataSourceArtifact(final String aUri, final String aName, final String aDescription, final String jndiName, final Long tenantId) {
		super(aUri, aName, aDescription, ArtifactType.JNDI_DATASOURCE, tenantId);
		changeJndiNameInternal(jndiName);
	}
	
	public String getJndiName() {
		return jndiName;
	}
	
	private void changeJndiNameInternal(final String aJndiName) {
		Assertions.hasText(aJndiName, "Datasource artifact jndi name must have a value.");
		Assertions.rangeLength(aJndiName, 1, 55, "Datasource artifact jndi name must be at least 1 character but not more than 55 characters.");
		this.jndiName = aJndiName;
	}
	
	public void changeJndiName(final String aJndiName, final Long version) {
		changeJndiNameInternal(aJndiName);
		EntityUtils.performConcurrencyCheck(this, version);
	}

}
