package org.shayvank.reportservice.domain.model;

public class ConcurrencyException extends RuntimeException {

	private static final long serialVersionUID = -6664760702653627919L;

	public ConcurrencyException(final String msg) {
		super(msg);
	}
	
	public ConcurrencyException(final String msg, final Throwable ex) {
		super(msg, ex);
	}
}
