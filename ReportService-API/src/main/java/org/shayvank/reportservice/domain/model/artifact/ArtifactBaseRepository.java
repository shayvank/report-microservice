package org.shayvank.reportservice.domain.model.artifact;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ArtifactBaseRepository<T extends Artifact> extends JpaRepository<T, Long> {
	
}
