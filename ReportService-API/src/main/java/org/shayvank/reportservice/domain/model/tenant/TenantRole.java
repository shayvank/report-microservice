package org.shayvank.reportservice.domain.model.tenant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import org.shayvank.reportservice.domain.model.EntityUtils;
import org.shayvank.reportservice.domain.model.Versioned;
import org.shayvank.reportservice.util.Assertions;

@Entity
@Table(name = "TENANT_ROLE")
public class TenantRole implements Versioned {

	@Id
	@GeneratedValue(
			generator = "tenantrole_sequence_id")
	@SequenceGenerator(
			name = "tenantrole_sequence_id",
			sequenceName = "TENANTROLE_SEQUENCE",
			allocationSize = 1)
	private Long id;
	
	@Column(name = "URI")
	private String uri;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "TENANT_ID")
	private Long tenantId;
	
	@Version
	@Column(name = "VERSION")
	private Long version;
	
	protected TenantRole() {}
	
	public TenantRole(final String uri, final String name, final String description, final Long tenantId) {
		changeUriInternal(uri);
		changeNameInternal(name);
		changeDescriptionInternal(description);
		this.tenantId = tenantId;
	}
	
	public Long id() {
		return id;
	}
	
	public String getUri() {
		return uri;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public Long getTenantId() {
		return tenantId;
	}
	
	@Override
	public Long getVersion() {
		return version;
	}
	
	private void changeUriInternal(final String aUri) {
		Assertions.hasText(aUri, "Tenant role uri must have a value.");
		Assertions.rangeLength(aUri, 1, 55, "Tenant role uri must be at least 1 character but not more than 55 characters.");
		this.uri = aUri;
	}
	
	public void changeUri(final String aUri, final Long version) {
		changeUriInternal(aUri);
		EntityUtils.performConcurrencyCheck(this, version);
	}
	
	private void changeNameInternal(final String aName) {
		Assertions.hasText(aName, "Tenant role name must have a value.");
		Assertions.rangeLength(aName, 1, 55, "Tenant role name must be at least 1 character but not more than 55 characters.");		
		this.name = aName;
	}
	
	public void changeName(final String aName, final Long version) {
		changeNameInternal(aName);
		EntityUtils.performConcurrencyCheck(this, version);
	}
	
	private void changeDescriptionInternal(final String aDescription) {
		Assertions.rangeLength(aDescription, 0, 128, "Tenant role description must not be more than 128 characters.");
		this.description = aDescription;
	}
	
	public void changeDescription(final String aDescription, final Long version) {
		changeDescriptionInternal(aDescription);
		EntityUtils.performConcurrencyCheck(this, version);
	}

}
