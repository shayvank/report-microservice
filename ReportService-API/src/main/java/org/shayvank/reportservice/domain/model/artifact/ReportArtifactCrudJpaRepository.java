package org.shayvank.reportservice.domain.model.artifact;

import java.util.List;

public interface ReportArtifactCrudJpaRepository extends ArtifactBaseRepository<ReportArtifact> {

	List<ReportArtifact> findByTenantId(Long tenantId);
	
	ReportArtifact findByUriAndTenantId(String uri, Long tenantId);
}
