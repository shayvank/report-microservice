package org.shayvank.reportservice.domain.model.artifact;

public enum ArtifactType {

	FILE("FL"),
	JNDI_DATASOURCE("JD"),
	REPORT("RT");
	
	private String code;
	
	private ArtifactType(final String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
}
