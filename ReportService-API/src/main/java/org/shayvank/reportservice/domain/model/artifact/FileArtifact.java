package org.shayvank.reportservice.domain.model.artifact;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.shayvank.reportservice.domain.model.EntityUtils;
import org.shayvank.reportservice.util.Assertions;

@Entity
@Table(name = "FILE_ARTIFACT")
@PrimaryKeyJoinColumn(name="ARTIFACT_ID")
public class FileArtifact extends Artifact {

	@Column(name = "FILE_NAME")
	private String fileName;
	
	@Lob
	@Column(name = "FILE_CONTENTS")
	private byte[] contents;
	
	protected FileArtifact() {}
	
	public FileArtifact(final String aUri, final String aName, final String aDescription, final String aFileName, final byte[] theContents, final Long tenantId) {
		super(aUri, aName, aDescription, ArtifactType.FILE, tenantId);
		changeFileInternal(aFileName, theContents);
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public byte[] getContents() {
		return contents;
	}
	
	private void changeFileInternal(final String aFileName, final byte[] theContents) {
		Assertions.hasText(aFileName, "File artifact file name must have a value.");
		Assertions.rangeLength(aFileName, 1, 55, "File artifact file name must be at least 1 character but not more than 55 characters.");
		if (theContents == null || theContents.length == 0) {
			throw new IllegalArgumentException("File contents must be provided.");
		}
		this.fileName = aFileName;
		this.contents = theContents;
	}
	
	public void changeFile(final String aFileName, final byte[] theContents, final Long version) {
		changeFileInternal(aFileName, theContents);
		EntityUtils.performConcurrencyCheck(this, version);
	}
}
