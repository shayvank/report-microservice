package org.shayvank.reportservice.domain.model.artifact;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.shayvank.reportservice.domain.model.EntityUtils;
import org.springframework.util.Assert;

@Entity
@Table(name = "REPORT_ARTIFACT")
@PrimaryKeyJoinColumn(name = "ARTIFACT_ID")
public class ReportArtifact extends FileArtifact {

	@ManyToOne
	@JoinColumn(name = "DATASOURCE_ID")
	private DataSourceArtifact datasource;
	
	protected ReportArtifact() {}
	
	public ReportArtifact(final String aUri, final String aName, final String aDescription, final String aFileName, final byte[] theContents, final Long tenantId, final DataSourceArtifact aDatasource) {
		super(aUri, aName, aDescription, aFileName, theContents, tenantId);
		this.artifactType = ArtifactType.REPORT;
		changeDataSourceInternal(aDatasource);
	}
	
	public DataSourceArtifact getDatasource() {
		return datasource;
	}
	
	private void changeDataSourceInternal(final DataSourceArtifact aDatasource) {
		Assert.notNull(aDatasource, "A datasource is required for a report.");
		this.datasource = aDatasource;
	}
	
	public void changeDatasource(final DataSourceArtifact datasource, final Long version) {
		changeDataSourceInternal(datasource);
		EntityUtils.performConcurrencyCheck(this, version);
	}
}
