package org.shayvank.reportservice.domain.model;

public class EntityUtils {

	private EntityUtils() {
		
	}
	
	public static void performConcurrencyCheck(final Versioned versioned, final Long version) {
		if (!versioned.getVersion().equals(version)) {
			throw new ConcurrencyException("Version out of sync: " + version.toString());
		}
	}
}
