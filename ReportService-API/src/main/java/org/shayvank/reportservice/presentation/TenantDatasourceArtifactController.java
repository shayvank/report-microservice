package org.shayvank.reportservice.presentation;

import java.util.List;
import java.util.Map;

import org.shayvank.reportservice.application.artifact.ArtifactApplicationService;
import org.shayvank.reportservice.application.artifact.ArtifactQueryService;
import org.shayvank.reportservice.application.artifact.DatasourceArtifactData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tenants/{tenanturi}/artifacts/datasources")
public class TenantDatasourceArtifactController extends BaseController {
	
	private ArtifactApplicationService artifactService;
	private ArtifactQueryService artifactQueryService;
	
	@Autowired
	public TenantDatasourceArtifactController(final ArtifactApplicationService artifactService,
			final ArtifactQueryService artifactQueryService) {
		this.artifactService = artifactService;
		this.artifactQueryService = artifactQueryService;
	}
	
	/*
	 /tenants/{tenanturi}/artifacts/datasources (GET) - gets the list of tenant datasources
	 /tenants/{tenanturi}/artifacts/datasources/{datasourceuri} (GET) - gets the specific tenant datasource
	 /tenants/{tenanturi}/artifacts/datasources (POST) - create a new datasource
	 /tenants/{tenanturi}/artifacts/datasources/{datasourceuri} (PATCH) - partially updates the tenant datasource
	 /tenants/{tenanturi}/artifacts/datasources/{datasourceuri} (DELETE) - deletes the tenant datasource
	 
	 // file artifact (pdf, html, img, etc)
	 
	 // jrxml
	 
	 
	 */

	@GetMapping()
	public List<DatasourceArtifactData> getTenantDatasourceList(@PathVariable("tenanturi") String tenanturi) {
		return artifactQueryService.listDatasourcesByTenant(tenanturi);
	}
	
	@GetMapping(path = "{datasourceuri}")
	public DatasourceArtifactData getTenantDatasourceByUri(@PathVariable("tenanturi") String tenanturi, @PathVariable("datasourceuri") String datasourceuri) {
		return artifactQueryService.queryDatasourceArtifactByUriAndTenantUri(datasourceuri, tenanturi);
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(consumes = "application/json")
	public DatasourceArtifactData createDatasource(@PathVariable("tenanturi") String tenanturi, @RequestBody final DatasourceArtifactData data) {
		return artifactService.newDataSourceArtifact(data, tenanturi);
	}
	
	@PatchMapping(path = "{datasourceuri}")
	public DatasourceArtifactData patchTenantDatasource(@PathVariable("tenanturi") String tenanturi, @PathVariable("datasourceuri") String datasourceuri, @RequestBody Map<String, Object> dataUpdates) {
		return artifactService.editDatasourceArtifact(datasourceuri, dataUpdates, tenanturi);
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping(path = "{datasourceuri}")
	public void deleteTenantDatasource(@PathVariable("tenanturi") String tenanturi, @PathVariable("datasourceuri") String datasourceuri) {
		artifactService.removeDatasourceArtifact(datasourceuri, tenanturi);
	}
}
