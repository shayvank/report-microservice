package org.shayvank.reportservice.presentation;

import java.util.List;
import java.util.Map;

import org.shayvank.reportservice.application.tenant.TenantApplicationService;
import org.shayvank.reportservice.application.tenant.TenantData;
import org.shayvank.reportservice.application.tenant.TenantQueryService;
import org.shayvank.reportservice.application.tenant.TenantRoleData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tenants")
public class TenantController extends BaseController {

	private TenantQueryService queryService;
	private TenantApplicationService appService;
	
	
	@Autowired
	public TenantController(final TenantQueryService queryService, final TenantApplicationService appService) {
		this.queryService = queryService;
		this.appService = appService;
	}
	
	
	/**
	 /tenants (GET) - gets the list of tenants
	 /tenants/{tenanturi} (GET) - gets the specific tenant
	 /tenants (POST) - creates a new tenant
	 /tenants/{tenanturi} (PATCH) - partially updates the tenant
	 /tenants/{tenanturi} (DELETE) - deletes the tenant
	   
	 /tenants/{tenanturi}/roles (GET) - gets the list of tenant roles
	 /tenants/{tenanturi}/roles/{roleuri} (GET) - gets the specific tenant role
	 /tenants/{tenanturi}/roles (POST) - adds a new role
	 /tenants/{tenanturi}/roles/{roleuri} (PATCH) - partially updates the tenant role
	 /tenants/{tenanturi}/roles/{roleuri} (DELETE) - deletes the tenant role
	 */
			
	@GetMapping()
	public List<TenantData> getTenantsList() {		
		return queryService.listAllTenants();
	}
	
	@GetMapping("/{tenanturi}")
	public TenantData getTenantById(@PathVariable("tenanturi") String tenanturi) {
		return queryService.queryTenantByUri(tenanturi);
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(consumes = "application/json")
	public TenantData createTenant(@RequestBody final TenantData data) {
		// given the tenant data, we will create a command to create a new tenant
		return appService.newTenant(data);		
	}
	
	@PatchMapping("/{tenanturi}")
	public TenantData updateTenantByUri(@PathVariable("tenanturi") String tenanturi, @RequestBody Map<String, Object> dataUpdates) {
		return appService.editTenant(tenanturi, dataUpdates);
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping("/tenanturi}")
	public void deleteTenantById(@PathVariable("tenanturi") String tenanturi) {
		appService.removeTenant(tenanturi);
	}
	
	@GetMapping("/{tenanturi}/roles")
	public List<TenantRoleData> getTenantRolesList(@PathVariable("tenanturi") String tenanturi) {
		return queryService.listRolesbyTenant(tenanturi);
	}
	
	@GetMapping("/{tenanturi}/roles/{roleuri}")
	public TenantRoleData getTenantRoleById(@PathVariable("tenanturi") String tenanturi, final @PathVariable("roleuri") String roleuri) {
		return queryService.queryTenantRoleByUriAndTenantUri(roleuri, tenanturi);
	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(path = "/{tenanturi}/roles", consumes = "application/json")
	public TenantRoleData createTenantRole(@PathVariable("tenanturi") String tenanturi, @RequestBody final TenantRoleData data) {
		// given the tenant data, we will create a command to create a new tenant
		return appService.newRole(data, tenanturi);		
	}
	
	@PatchMapping("/{tenanturi}/roles/{roleuri}")
	public TenantRoleData updateTenantRoleById(@PathVariable("tenanturi") String tenanturi, final @PathVariable("roleuri") String roleuri, @RequestBody Map<String, Object> dataUpdates) {
		return appService.editTenantRole(roleuri, dataUpdates, tenanturi);
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping("/{tenanturi}/roles/{roleuri}")
	public void deleteTenantRoleById(@PathVariable("tenanturi") String tenanturi, final @PathVariable("roleuri") String roleuri) {
		appService.removeTenantRole(roleuri, tenanturi);
	}
	
}
