package org.shayvank.reportservice.presentation;

import org.shayvank.reportservice.application.artifact.FileArtifactData;
import org.shayvank.reportservice.application.artifact.ReportArtifactData;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CreateArtifactFileDto {
	
	private MultipartFile file;

	private String artifact;
	
	private String datasourceUri;

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getArtifact() {
		return artifact;
	}

	public void setArtifact(String artifact) {
		this.artifact = artifact;
	}
	
	public String getDatasourceUri() {
		return datasourceUri;
	}
	
	public void setDatasourceUri(String datasourceUri) {
		this.datasourceUri = datasourceUri;
	}
	
	public FileArtifactData getFileArtifactData() throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(artifact, FileArtifactData.class);
	}
	
	public ReportArtifactData getReportArtifactData() throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		ReportArtifactData data = objectMapper.readValue(artifact, ReportArtifactData.class);
		data.setDatasourceUri(datasourceUri);
		return data;
	}
	
}
