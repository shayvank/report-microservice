package org.shayvank.reportservice.presentation;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

public class JsonErrorResponse {

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
	private LocalDateTime timestamp;
	
	private int status;
	
	private String error;
	
	public JsonErrorResponse(final int status, final String error) {
		this.status = status;
		this.error = error;
		this.timestamp = LocalDateTime.now();
	}
	
	public String getError() {
		return error;
	}
	
	public int getStatus() {
		return status;
	}
	
	public LocalDateTime getTimestamp() {
		return timestamp;
	}
}
