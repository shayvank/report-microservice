package org.shayvank.reportservice.presentation;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.shayvank.reportservice.application.artifact.ArtifactApplicationService;
import org.shayvank.reportservice.application.artifact.ArtifactQueryService;
import org.shayvank.reportservice.application.artifact.FileArtifactData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/tenants/{tenanturi}/artifacts/files")
public class TenantFileArtifactController extends BaseController {
	
	private ArtifactApplicationService artifactService;
	private ArtifactQueryService artifactQueryService;
	
	@Autowired
	public TenantFileArtifactController(final ArtifactApplicationService artifactService,
			final ArtifactQueryService artifactQueryService) {
		this.artifactService = artifactService;
		this.artifactQueryService = artifactQueryService;
	}
	
	/*
	 /tenants/{tenanturi}/artifacts/files (GET) - gets the list of tenant files
	 /tenants/{tenanturi}/artifacts/files/{fileuri} (GET) - gets the specific tenant file
	 /tenants/{tenanturi}/artifacts/files/{fileuri}/download
	 /tenants/{tenanturi}/artifacts/files (POST) - create a new file
	 /tenants/{tenanturi}/artifacts/files/{fileuri} (PATCH) - partially updates the tenant file
	 /tenants/{tenanturi}/artifacts/files/{fileuri} (DELETE) - deletes the tenant file	 
	 */

	@GetMapping()
	public List<FileArtifactData> getTenantFileList(@PathVariable("tenanturi") String tenanturi) {
		return artifactQueryService.listFilesByTenant(tenanturi);
	}
	
	@GetMapping(path = "{fileuri}")
	public FileArtifactData getTenantFileByUri(@PathVariable("tenanturi") String tenanturi, @PathVariable("fileuri") String fileuri) {
		return artifactQueryService.queryFileArtifactByUriAndTenantUri(fileuri, tenanturi);
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(consumes = {"multipart/form-data"})
	public FileArtifactData createFile(@PathVariable("tenanturi") String tenanturi, @ModelAttribute CreateArtifactFileDto dtoModel) throws IOException {
		FileArtifactData data = dtoModel.getFileArtifactData();
		MultipartFile file = dtoModel.getFile();
		data.setFileName(file.getOriginalFilename());
		return artifactService.newFileArtifact(data, file.getBytes(), tenanturi);
	}
	
	@PatchMapping(path = "{fileuri}")
	public FileArtifactData patchTenantFile(@PathVariable("tenanturi") String tenanturi, @PathVariable("fileuri") String fileuri, @RequestBody Map<String, Object> dataUpdates) {
		return artifactService.editFileArtifact(fileuri, dataUpdates, tenanturi);
	}
	
	@PostMapping(path = "{fileuri}/upload", consumes = {"multipart/form-data"})
	public FileArtifactData updateFile(@PathVariable("tenanturi") String tenanturi, @PathVariable("fileuri") String fileuri, @RequestPart("version") Long version, @RequestPart("file") MultipartFile file) throws IOException {
		return artifactService.updateFileArtifactContents(fileuri, file.getOriginalFilename(), file.getBytes(), version, tenanturi);
	}	
	
	@GetMapping(path = "{fileuri}/download",
			produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public @ResponseBody byte[] getFileContents(@PathVariable("tenanturi") String tenanturi, @PathVariable("fileuri") String fileuri) {
		return artifactQueryService.queryFileContents(fileuri, tenanturi);
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping(path = "{fileuri}")
	public void deleteTenantFile(@PathVariable("tenanturi") String tenanturi, @PathVariable("fileuri") String fileuri) {
		artifactService.removeFileArtifact(fileuri, tenanturi);
	}
}
