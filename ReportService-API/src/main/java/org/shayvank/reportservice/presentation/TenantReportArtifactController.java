package org.shayvank.reportservice.presentation;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.shayvank.reportservice.application.artifact.ArtifactApplicationService;
import org.shayvank.reportservice.application.artifact.ArtifactQueryService;
import org.shayvank.reportservice.application.artifact.ReportArtifactData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/tenants/{tenanturi}/artifacts/reports")
public class TenantReportArtifactController extends BaseController {
	
	private ArtifactApplicationService artifactService;
	private ArtifactQueryService artifactQueryService;
	
	@Autowired
	public TenantReportArtifactController(final ArtifactApplicationService artifactService,
			final ArtifactQueryService artifactQueryService) {
		this.artifactService = artifactService;
		this.artifactQueryService = artifactQueryService;
	}
	
	/*
	 /tenants/{tenanturi}/artifacts/reports (GET) - gets the list of tenant reports
	 /tenants/{tenanturi}/artifacts/reports/{fileuri} (GET) - gets the specific tenant report
	 /tenants/{tenanturi}/artifacts/reports/{fileuri}/download
	 /tenants/{tenanturi}/artifacts/reports (POST) - create a new report
	 /tenants/{tenanturi}/artifacts/reports/{fileuri} (PATCH) - partially updates the tenant report
	 /tenants/{tenanturi}/artifacts/reports/{fileuri} (DELETE) - deletes the tenant report	 
	 */

	@GetMapping()
	public List<ReportArtifactData> getTenantReportList(@PathVariable("tenanturi") String tenanturi) {
		return artifactQueryService.listReportsByTenant(tenanturi);
	}
	
	@GetMapping(path = "{reporturi}")
	public ReportArtifactData getTenantFileByUri(@PathVariable("tenanturi") String tenanturi, @PathVariable("reporturi") String reporturi) {
		return artifactQueryService.queryReportArtifactByUriAndTenantUri(reporturi, tenanturi);
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(consumes = {"multipart/form-data"})
	public ReportArtifactData createFile(@PathVariable("tenanturi") String tenanturi, @ModelAttribute CreateArtifactFileDto dtoModel) throws IOException {
		ReportArtifactData data = dtoModel.getReportArtifactData();
		MultipartFile file = dtoModel.getFile();
		data.setFileName(file.getOriginalFilename());
		return artifactService.newReportArtifact(data, file.getBytes(), tenanturi);
	}
	
	@PatchMapping(path = "{reporturi}")
	public ReportArtifactData patchTenantFile(@PathVariable("tenanturi") String tenanturi, @PathVariable("reporturi") String reporturi, @RequestBody Map<String, Object> dataUpdates) {
		return artifactService.editReportArtifact(reporturi, dataUpdates, tenanturi);
	}
	
	@PostMapping(path = "{reporturi}/upload", consumes = {"multipart/form-data"})
	public ReportArtifactData updateFile(@PathVariable("tenanturi") String tenanturi, @PathVariable("reporturi") String reporturi, @RequestPart("version") Long version, @RequestPart("file") MultipartFile file) throws IOException {
		return artifactService.updateReportArtifactContents(reporturi, file.getOriginalFilename(), file.getBytes(), version, tenanturi);
	}	
	
	@GetMapping(path = "{reporturi}/download",
			produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public @ResponseBody byte[] getFileContents(@PathVariable("tenanturi") String tenanturi, @PathVariable("reporturi") String reporturi) {
		return artifactQueryService.queryReportContents(reporturi, tenanturi);
	}
	
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@DeleteMapping(path = "{reporturi}")
	public void deleteTenantFile(@PathVariable("tenanturi") String tenanturi, @PathVariable("reporturi") String reporturi) {
		artifactService.removeReportArtifact(reporturi, tenanturi);
	}
}
