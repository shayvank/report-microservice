package org.shayvank.reportservice.presentation;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.shayvank.reportservice.application.NotFoundException;
import org.shayvank.reportservice.domain.model.ConcurrencyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class BaseController {

	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<JsonErrorResponse> handleNotFoundException(NotFoundException e) {
		return convertExceptionToErrorResponse(e, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(ConcurrencyException.class)
	public ResponseEntity<JsonErrorResponse> handleConcurrencyException(ConcurrencyException e) {
		return convertExceptionToErrorResponse(e, HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<JsonErrorResponse> handleRuntimeException(RuntimeException e) {
		return convertExceptionToErrorResponse(e, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	private ResponseEntity<JsonErrorResponse> convertExceptionToErrorResponse(final Exception ex, final HttpStatus status) {
		ex.printStackTrace();
		return new ResponseEntity<>(new JsonErrorResponse(status.value(), ex.getMessage()), status);
	}
}
