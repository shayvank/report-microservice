package org.shayvank.reportservice.application.tenant;

import java.util.Map;

import javax.transaction.Transactional;

import org.shayvank.reportservice.application.AppUtils;
import org.shayvank.reportservice.domain.model.tenant.Tenant;
import org.shayvank.reportservice.domain.model.tenant.TenantCrudJpaRepository;
import org.shayvank.reportservice.domain.model.tenant.TenantRole;
import org.shayvank.reportservice.domain.model.tenant.TenantRoleCrudJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class TenantApplicationService {

	private TenantCrudJpaRepository tenantRepo;
	private TenantRoleCrudJpaRepository tenantRoleRepo;
	
	@Autowired
	public TenantApplicationService(final TenantCrudJpaRepository tenantRepo, 
			final TenantRoleCrudJpaRepository tenantRoleRepo) {
		this.tenantRepo = tenantRepo;
		this.tenantRoleRepo = tenantRoleRepo;
	}
	
	public TenantData newTenant(final TenantData tenantData) {
		Tenant tenant = new Tenant(tenantData.getUri(), tenantData.getName(), tenantData.getDescription());
		tenantRepo.save(tenant);
		return new TenantData(tenant);
	}
	
	public TenantData editTenant(final String tenantUri, final Map<String, Object> dataUpdates) {
		Long version = AppUtils.getVersion(dataUpdates);
		
		Tenant tenant = tenantRepo.findByUri(tenantUri);
		
		if (dataUpdates.containsKey("uri")) {
			tenant.changeUri((String) dataUpdates.get("uri"), version);
		}
		if (dataUpdates.containsKey("name")) {
			tenant.changeName((String) dataUpdates.get("name"), version);
		}
		if (dataUpdates.containsKey("description")) {
			tenant.changeDescription((String) dataUpdates.get("description"), version);
		}

		tenantRepo.save(tenant);
		
		return new TenantData(tenant);
	}
	
	public void removeTenant(final String tenantUri) {
		Tenant tenant = tenantRepo.findByUri(tenantUri);	
		tenantRepo.deleteById(tenant.id());
	}
	
	public TenantRoleData newRole(final TenantRoleData roleData, final String tenantUri) {
		Tenant tenant = tenantRepo.findByUri(tenantUri);
		
		TenantRole role = new TenantRole(roleData.getUri(), roleData.getName(), roleData.getDescription(), tenant.id());
		
		tenantRoleRepo.save(role);
		return new TenantRoleData(role);
	}
	
	public TenantRoleData editTenantRole(final String roleUri, final Map<String, Object> dataUpdates, final String tenantUri) {
		Long version = AppUtils.getVersion(dataUpdates);
		
		TenantRole role = tenantRoleRepo.findByUriAndTenantId(roleUri, tenantRepo.findByUri(tenantUri).id());
		if (dataUpdates.containsKey("uri")) {
			role.changeUri((String) dataUpdates.get("name"), version);
		}
		if (dataUpdates.containsKey("name")) {
			role.changeName((String) dataUpdates.get("name"), version);
		}
		if (dataUpdates.containsKey("description")) {
			role.changeDescription((String) dataUpdates.get("description"), version);
		}
		tenantRoleRepo.save(role);
		
		return new TenantRoleData(role);
	}
	
	public void removeTenantRole(final String roleUri, final String tenantUri) {	
		TenantRole role = tenantRoleRepo.findByUriAndTenantId(roleUri, tenantRepo.findByUri(tenantUri).id());
		
		tenantRoleRepo.deleteById(role.id());		
	}

}
