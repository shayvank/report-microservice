package org.shayvank.reportservice.application.artifact;

import java.util.List;
import java.util.stream.Collectors;

import org.shayvank.reportservice.application.NotFoundException;
import org.shayvank.reportservice.domain.model.artifact.DataSourceArtifact;
import org.shayvank.reportservice.domain.model.artifact.DataSourceArtifactCrudJpaRepository;
import org.shayvank.reportservice.domain.model.artifact.FileArtifact;
import org.shayvank.reportservice.domain.model.artifact.FileArtifactCrudJpaRepository;
import org.shayvank.reportservice.domain.model.artifact.ReportArtifact;
import org.shayvank.reportservice.domain.model.artifact.ReportArtifactCrudJpaRepository;
import org.shayvank.reportservice.domain.model.tenant.Tenant;
import org.shayvank.reportservice.domain.model.tenant.TenantCrudJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArtifactQueryService {
	
	private TenantCrudJpaRepository tenantRepository;
	private DataSourceArtifactCrudJpaRepository datasourceCrudRepo;
	private FileArtifactCrudJpaRepository fileCrudRepo;
	private ReportArtifactCrudJpaRepository reportCrudRepo;
	
	@Autowired
	public ArtifactQueryService(
			final TenantCrudJpaRepository tenantRepository,
			final DataSourceArtifactCrudJpaRepository datasourceCrudRepo,
			final FileArtifactCrudJpaRepository fileCrudRepo,
			final ReportArtifactCrudJpaRepository reportCrudRepo) {
		this.tenantRepository = tenantRepository;
		this.datasourceCrudRepo = datasourceCrudRepo;
		this.fileCrudRepo = fileCrudRepo;
		this.reportCrudRepo = reportCrudRepo;
	}
	
	public List<DatasourceArtifactData> listDatasourcesByTenant(final String tenantUri) {
		Tenant tenant = tenantRepository.findByUri(tenantUri);
	
		return datasourceCrudRepo.findByTenantId(tenant.id()).stream()
				.map(ent-> new DatasourceArtifactData(ent))
				.collect(Collectors.toList());
	}
	
	public DatasourceArtifactData queryDatasourceArtifactByUriAndTenantUri(final String datasourceUri, final String tenantUri) {
		// first, we need to convert the tenanturi to the tenandid
		Tenant tenant = tenantRepository.findByUri(tenantUri);
		if (tenant == null) {
			throw new NotFoundException("Tenant not found by uri.");
		}
		
		DataSourceArtifact artifact = datasourceCrudRepo.findByUriAndTenantId(datasourceUri, tenant.id());
		if (artifact == null) {
			throw new NotFoundException("Datasource artifact not found by uri");
		}
		return new DatasourceArtifactData(artifact);
	}
	
	public List<FileArtifactData> listFilesByTenant(final String tenantUri) {
		Tenant tenant = tenantRepository.findByUri(tenantUri);
	
		return fileCrudRepo.findByTenantId(tenant.id()).stream()
				.map(ent-> new FileArtifactData(ent))
				.collect(Collectors.toList());
	}
	
	public FileArtifactData queryFileArtifactByUriAndTenantUri(final String fileUri, final String tenantUri) {
		// first, we need to convert the tenanturi to the tenandid
		Tenant tenant = tenantRepository.findByUri(tenantUri);
		if (tenant == null) {
			throw new NotFoundException("Tenant not found by uri.");
		}
		
		FileArtifact artifact = fileCrudRepo.findByUriAndTenantId(fileUri, tenant.id());
		if (artifact == null) {
			throw new NotFoundException("File artifact not found by uri");
		}
		return new FileArtifactData(artifact);
	}
	
	public byte[] queryFileContents(final String fileUri, final String tenantUri) {
		// first, we need to convert the tenanturi to the tenandid
		Tenant tenant = tenantRepository.findByUri(tenantUri);
		if (tenant == null) {
			throw new NotFoundException("Tenant not found by uri.");
		}
		
		FileArtifact artifact = fileCrudRepo.findByUriAndTenantId(fileUri, tenant.id());
		if (artifact == null) {
			throw new NotFoundException("File artifact not found by uri");
		}
		return artifact.getContents();
	}
	
	public List<ReportArtifactData> listReportsByTenant(final String tenantUri) {
		Tenant tenant = tenantRepository.findByUri(tenantUri);
	
		return reportCrudRepo.findByTenantId(tenant.id()).stream()
				.map(ent-> new ReportArtifactData(ent))
				.collect(Collectors.toList());
	}
	
	public ReportArtifactData queryReportArtifactByUriAndTenantUri(final String reportUri, final String tenantUri) {
		// first, we need to convert the tenanturi to the tenandid
		Tenant tenant = tenantRepository.findByUri(tenantUri);
		if (tenant == null) {
			throw new NotFoundException("Tenant not found by uri.");
		}
		
		ReportArtifact artifact = reportCrudRepo.findByUriAndTenantId(reportUri, tenant.id());
		if (artifact == null) {
			throw new NotFoundException("Report artifact not found by uri");
		}
		return new ReportArtifactData(artifact);
	}
	
	public byte[] queryReportContents(final String reportUri, final String tenantUri) {
		// first, we need to convert the tenanturi to the tenandid
		Tenant tenant = tenantRepository.findByUri(tenantUri);
		if (tenant == null) {
			throw new NotFoundException("Tenant not found by uri.");
		}
		
		ReportArtifact artifact = reportCrudRepo.findByUriAndTenantId(reportUri, tenant.id());
		if (artifact == null) {
			throw new NotFoundException("Report artifact not found by uri");
		}
		return artifact.getContents();
	}	
}
