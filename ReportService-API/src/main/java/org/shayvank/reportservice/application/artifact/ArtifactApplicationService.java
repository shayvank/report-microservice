package org.shayvank.reportservice.application.artifact;

import java.util.Map;

import javax.transaction.Transactional;

import org.shayvank.reportservice.application.AppUtils;
import org.shayvank.reportservice.domain.model.artifact.Artifact;
import org.shayvank.reportservice.domain.model.artifact.DataSourceArtifact;
import org.shayvank.reportservice.domain.model.artifact.DataSourceArtifactCrudJpaRepository;
import org.shayvank.reportservice.domain.model.artifact.FileArtifact;
import org.shayvank.reportservice.domain.model.artifact.FileArtifactCrudJpaRepository;
import org.shayvank.reportservice.domain.model.artifact.ReportArtifact;
import org.shayvank.reportservice.domain.model.artifact.ReportArtifactCrudJpaRepository;
import org.shayvank.reportservice.domain.model.tenant.Tenant;
import org.shayvank.reportservice.domain.model.tenant.TenantCrudJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class ArtifactApplicationService {

	private TenantCrudJpaRepository tenantRepo;
	private DataSourceArtifactCrudJpaRepository dataSourceCrudRepo;
	private FileArtifactCrudJpaRepository fileCrudRepo;
	private ReportArtifactCrudJpaRepository reportCrudRepo;
	
	@Autowired
	public ArtifactApplicationService(final TenantCrudJpaRepository tenantRepo,
			final DataSourceArtifactCrudJpaRepository dataSourceCrudRepo,
			final FileArtifactCrudJpaRepository fileCrudRepo,
			final ReportArtifactCrudJpaRepository reportCrudRepo) {
		this.tenantRepo = tenantRepo;
		this.dataSourceCrudRepo = dataSourceCrudRepo;
		this.fileCrudRepo = fileCrudRepo;
		this.reportCrudRepo = reportCrudRepo;
	}
	
	public DatasourceArtifactData newDataSourceArtifact(final DatasourceArtifactData dsData, final String tenantUri) {		
		Tenant tenant = tenantRepo.findByUri(tenantUri);
		
		DataSourceArtifact artifact = new DataSourceArtifact(dsData.getUri(), dsData.getName(),
				dsData.getDescription(), dsData.getJndiName(), tenant.id());
		dataSourceCrudRepo.save(artifact);
		
		return new DatasourceArtifactData(artifact);
	}
	
	public DatasourceArtifactData editDatasourceArtifact(final String datasourceUri, final Map<String, Object> dataUpdates, final String tenantUri) {						
		DataSourceArtifact artifact = dataSourceCrudRepo.findByUriAndTenantId(datasourceUri, tenantRepo.findByUri(tenantUri).id());
		
		patchArtifact(artifact, dataUpdates);
		
		if (dataUpdates.containsKey("jndiName")) {
			Long version = AppUtils.getVersion(dataUpdates);
			artifact.changeName((String) dataUpdates.get("jndiName"), version);
		}
		
		dataSourceCrudRepo.save(artifact);
		
		return new DatasourceArtifactData(artifact);
		
	}
	
	public void removeDatasourceArtifact(final String datasourceUri, final String tenantUri) {		
		DataSourceArtifact artifact = dataSourceCrudRepo.findByUriAndTenantId(datasourceUri, tenantRepo.findByUri(tenantUri).id());
		
		dataSourceCrudRepo.deleteById(artifact.id());		
	}
	
	public FileArtifactData newFileArtifact(final FileArtifactData fileData, final byte[] contents, final String tenantUri) {		
		Tenant tenant = tenantRepo.findByUri(tenantUri);
		
		FileArtifact artifact = new FileArtifact(fileData.getUri(), fileData.getName(),
				fileData.getDescription(), fileData.getFileName(), contents, tenant.id());
		fileCrudRepo.save(artifact);
		
		return new FileArtifactData(artifact);
	}
	
	public FileArtifactData editFileArtifact(final String fileUri, final Map<String, Object> dataUpdates, final String tenantUri) {						
		FileArtifact artifact = fileCrudRepo.findByUriAndTenantId(fileUri, tenantRepo.findByUri(tenantUri).id());
		
		patchArtifact(artifact, dataUpdates);
		
		fileCrudRepo.save(artifact);
		
		return new FileArtifactData(artifact);		
	}
	
	public FileArtifactData updateFileArtifactContents(final String fileUri, final String fileName, final byte[] contents, final Long version, final String tenantUri) {
		FileArtifact artifact = fileCrudRepo.findByUriAndTenantId(fileUri, tenantRepo.findByUri(tenantUri).id());
		
		artifact.changeFile(fileName, contents, version);
		
		fileCrudRepo.save(artifact);
		
		return new FileArtifactData(artifact);
	}
	
	public void removeFileArtifact(final String fileUri, final String tenantUri) {		
		FileArtifact artifact = fileCrudRepo.findByUriAndTenantId(fileUri, tenantRepo.findByUri(tenantUri).id());
		
		fileCrudRepo.deleteById(artifact.id());		
	}
	
	public ReportArtifactData newReportArtifact(final ReportArtifactData fileData, final byte[] contents, final String tenantUri) {		
		Tenant tenant = tenantRepo.findByUri(tenantUri);
		
		DataSourceArtifact datasource = dataSourceCrudRepo.findByUriAndTenantId(fileData.getDatasourceUri(), tenant.id());
		
		ReportArtifact artifact = new ReportArtifact(fileData.getUri(), fileData.getName(),
				fileData.getDescription(), fileData.getFileName(), contents, tenant.id(), datasource);
		reportCrudRepo.save(artifact);
		
		return new ReportArtifactData(artifact);
	}
	
	public ReportArtifactData editReportArtifact(final String reportUri, final Map<String, Object> dataUpdates, final String tenantUri) {		
		Long tenantId = tenantRepo.findByUri(tenantUri).id();
		ReportArtifact artifact = reportCrudRepo.findByUriAndTenantId(reportUri, tenantId);
		
		patchArtifact(artifact, dataUpdates);
		
		if (dataUpdates.containsKey("datasourceUri")) {
			Long version = AppUtils.getVersion(dataUpdates);
			artifact.changeDatasource(dataSourceCrudRepo.findByUriAndTenantId((String) dataUpdates.get("datasourceUri"), tenantId), version);
		}
		
		reportCrudRepo.save(artifact);
		
		return new ReportArtifactData(artifact);		
	}
	
	public ReportArtifactData updateReportArtifactContents(final String reportUri, final String fileName, final byte[] contents, final Long version, final String tenantUri) {
		ReportArtifact artifact = reportCrudRepo.findByUriAndTenantId(reportUri, tenantRepo.findByUri(tenantUri).id());
		
		artifact.changeFile(fileName, contents, version);
		
		reportCrudRepo.save(artifact);
		
		return new ReportArtifactData(artifact);
	}
	
	public void removeReportArtifact(final String reportUri, final String tenantUri) {		
		ReportArtifact artifact = reportCrudRepo.findByUriAndTenantId(reportUri, tenantRepo.findByUri(tenantUri).id());
		
		reportCrudRepo.deleteById(artifact.id());		
	}	

	private void patchArtifact(final Artifact artifact, final Map<String, Object> dataUpdates) {
		Long version = AppUtils.getVersion(dataUpdates);
		
		if (dataUpdates.containsKey("uri")) {
			artifact.changeUri((String) dataUpdates.get("uri"), version);
		}
		
		if (dataUpdates.containsKey("name")) {
			artifact.changeName((String) dataUpdates.get("name"), version);
		}
		
		if (dataUpdates.containsKey("description")) {
			artifact.changeDescription((String) dataUpdates.get("description"), version);
		}
	}
}
