package org.shayvank.reportservice.application;

import java.util.Map;

public class AppUtils {

	private static final String VERSION_NAME = "version";
	
	private AppUtils() { }
	
	public static Long getVersion(final Map<String, Object> patchMap) {
		Long version = null;
		try {
			Object mapValue = patchMap.get(VERSION_NAME);
			if (mapValue instanceof Long) {
				version = (Long) mapValue;
			} else {
				version = Long.parseLong(((String) mapValue));
			}
		} catch (Exception e) {
			throw new MissingVersionKeyException("Missing version key.");
		}
		return version;
	}
}
