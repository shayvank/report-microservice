package org.shayvank.reportservice.application;

public class MissingVersionKeyException extends RuntimeException {

	private static final long serialVersionUID = -6664760702653627919L;

	public MissingVersionKeyException(final String msg) {
		super(msg);
	}
	
	public MissingVersionKeyException(final String msg, final Throwable ex) {
		super(msg, ex);
	}
}
