package org.shayvank.reportservice.application.artifact;

import org.shayvank.reportservice.domain.model.artifact.Artifact;

public abstract class ArtifactData {

	private String uri;
	private String name;
	private String description;
	private Long version;
	
	protected ArtifactData() {
		
	}
	
	protected ArtifactData(final Artifact artifact) {
		this.uri = artifact.uri();
		this.name = artifact.name();
		this.description = artifact.description();
		this.version = artifact.getVersion();
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
	
	
}
