package org.shayvank.reportservice.application.tenant;

import org.shayvank.reportservice.domain.model.tenant.Tenant;

public class TenantData {

	private String uri;
	private String name;
	private String description;
	private Long version;
	
	public TenantData() {
		
	}
	
	public TenantData(final Tenant tenant) {
		this.uri = tenant.uri();
		this.name = tenant.name();
		this.description = tenant.description();
		this.version = tenant.getVersion();
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	
	
}
