package org.shayvank.reportservice.application.artifact;

import org.shayvank.reportservice.domain.model.artifact.DataSourceArtifact;

public class DatasourceArtifactData extends ArtifactData {

	private String jndiName;
	
	public DatasourceArtifactData() {
		super();
	}
	
	public DatasourceArtifactData(final DataSourceArtifact artifact) {
		super(artifact);
		this.jndiName = artifact.getJndiName();
	}
	
	public String getJndiName() {
		return jndiName;
	}
	
	public void setJndiName(String jndiName) {
		this.jndiName = jndiName;
	}
}
