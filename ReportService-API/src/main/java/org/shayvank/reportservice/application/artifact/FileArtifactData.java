package org.shayvank.reportservice.application.artifact;

import org.shayvank.reportservice.domain.model.artifact.FileArtifact;

public class FileArtifactData extends ArtifactData {
	
	private String fileName;

	public FileArtifactData() {
		super();
	}
	
	public FileArtifactData(final FileArtifact artifact) {
		super(artifact);
		this.fileName = artifact.getFileName();
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}
}
