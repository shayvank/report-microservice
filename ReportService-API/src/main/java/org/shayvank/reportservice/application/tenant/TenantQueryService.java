package org.shayvank.reportservice.application.tenant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.shayvank.reportservice.application.NotFoundException;
import org.shayvank.reportservice.domain.model.tenant.Tenant;
import org.shayvank.reportservice.domain.model.tenant.TenantCrudJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

@Service
public class TenantQueryService {

	private static final String TENANT_BASE_QUERY = "select ID, NAME, URI, DESCRIPTION, VERSION from TENANT";
	private static final String TENANTROLE_BASE_QUERY = "select a.ID, a.URI, a.NAME, a.DESCRIPTION, a.VERSION, b.URI as TENANTURI from TENANT_ROLE a JOIN tenant b on a.TENANT_ID = b.ID";
	
	private JdbcTemplate jdbcTemplate;
	private TenantCrudJpaRepository tenantCrudRepo;
	
	@Autowired
	public TenantQueryService(final JdbcTemplate jdbcTemplate, final TenantCrudJpaRepository tenantCrudRepo) {
		this.jdbcTemplate = jdbcTemplate;
		this.tenantCrudRepo = tenantCrudRepo;
	}
	
	public List<TenantData> listAllTenants() {
		String query = TENANT_BASE_QUERY + " order by id";
		return jdbcTemplate.query(query, new ResultSetExtractor<List<TenantData>>() {

			@Override
			public List<TenantData> extractData(ResultSet rs) throws SQLException {
				List<TenantData> data = new ArrayList<>();
				
				while (rs.next()) {
					data.add(rsToTenantData(rs));
				}
				
				return data;
			}
			
		});
	}
	
	public TenantData queryById(final Long tenantId) {
		Tenant tenant = tenantCrudRepo.findById(tenantId).orElse(null);
		if (tenant == null) {
			throw new NotFoundException("Tenant not found by id.");
		}
		return new TenantData(tenant);	
	}
	
	public TenantData queryTenantByUri(final String tenantUri) {
		Tenant tenant = tenantCrudRepo.findByUri(tenantUri);
		if (tenant == null) {
			throw new NotFoundException("Tenant not found by uri.");
		}
		return new TenantData(tenant);
	}
	
	public TenantData queryByName(final String tenantName) {
		Tenant tenant = tenantCrudRepo.findByName(tenantName);
		if (tenant == null) {
			throw new NotFoundException("Tenant not found by name.");
		}
		return new TenantData(tenant);	
	}
	
	private TenantData rsToTenantData(final ResultSet rs) throws SQLException {
		TenantData tData = new TenantData();
		tData.setName(rs.getString("NAME"));
		tData.setUri(rs.getString("URI"));
		tData.setDescription(rs.getString("DESCRIPTION"));
		tData.setVersion(rs.getLong("VERSION"));
		return tData;
	}
	
	public List<TenantRoleData> listRolesbyTenant(final String tenantUri) {
		String query = TENANTROLE_BASE_QUERY + " where b.URI = ? order by a.NAME";
		return jdbcTemplate.query(query, new ResultSetExtractor<List<TenantRoleData>>() {

			@Override
			public List<TenantRoleData> extractData(ResultSet rs) throws SQLException {
				List<TenantRoleData> data = new ArrayList<>();
				
				while (rs.next()) {
					data.add(rsToTenantRoleData(rs));
				}
				
				return data;
			}
			
		}, tenantUri);
	}
	
	public TenantRoleData queryTenantRoleById(final Long id) {
		String query = TENANTROLE_BASE_QUERY + " where a.ID = ?";
		try {
			return jdbcTemplate.queryForObject(query, new RowMapper<TenantRoleData>() {
	
				@Override
				public TenantRoleData mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rsToTenantRoleData(rs);
				}
				
			}, id);
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException("Tenant role not found by id.");
		}
	}
	
	public TenantRoleData queryTenantRoleByUriAndTenantUri(final String roleUri, final String tenantUri) {
		String query = TENANTROLE_BASE_QUERY + " where a.URI = ? and b.URI = ?";
		try {
			return jdbcTemplate.queryForObject(query, new RowMapper<TenantRoleData>() {
	
				@Override
				public TenantRoleData mapRow(ResultSet rs, int rowNum) throws SQLException {
					return rsToTenantRoleData(rs);
				}
				
			}, roleUri, tenantUri);
		} catch (EmptyResultDataAccessException e) {
			throw new NotFoundException("Tenant role not found by role uri and tenant uri.");
		}
	}
	
	private TenantRoleData rsToTenantRoleData(final ResultSet rs) throws SQLException {
		TenantRoleData tData = new TenantRoleData();
		tData.setUri(rs.getString("URI"));
		tData.setName(rs.getString("NAME"));
		tData.setDescription(rs.getString("DESCRIPTION"));
		tData.setVersion(rs.getLong("VERSION"));
		return tData;
	}
}
