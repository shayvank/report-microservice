package org.shayvank.reportservice.application.artifact;

import org.shayvank.reportservice.domain.model.artifact.ReportArtifact;

public class ReportArtifactData extends FileArtifactData {

	private String datasourceUri;	
	
	public ReportArtifactData() {
		super();
	}
	
	public ReportArtifactData(final ReportArtifact ent) {
		super(ent);
		this.datasourceUri = ent.getDatasource().uri();
	}
	
	public String getDatasourceUri() {
		return datasourceUri;
	}
	
	public void setDatasourceUri(final String datasourceUri) {
		this.datasourceUri = datasourceUri;
	}
}
