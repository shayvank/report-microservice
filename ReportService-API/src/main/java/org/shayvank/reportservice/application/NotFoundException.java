package org.shayvank.reportservice.application;

public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = -6664760702653627919L;

	public NotFoundException(final String msg) {
		super(msg);
	}
	
	public NotFoundException(final String msg, final Throwable ex) {
		super(msg, ex);
	}
}
