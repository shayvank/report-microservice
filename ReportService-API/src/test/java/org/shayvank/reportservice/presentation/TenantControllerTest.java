package org.shayvank.reportservice.presentation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.shayvank.reportservice.application.tenant.TenantApplicationService;
import org.shayvank.reportservice.application.tenant.TenantQueryService;

@RunWith(SpringRunner.class)
@WebMvcTest(TenantController.class)
public class TenantControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private TenantQueryService queryService;
	
	@MockBean
	private TenantApplicationService appService;
	
	@Test
	public void test_getTenantsList() throws Exception {
		mockMvc.perform(
			get("/"))
			.andDo(print())
			.andExpect(status().isOk());
	}
}
